# Check JS

by Deep Upadhyay <deepchandra84@gmail.com>

## Contents
  
  * Introduction
  * Features
  * Requirements
  * Installation
  
## Introduction

This module checks if javascript is enabled or supported in the browser.

## Features

1. This module checks if javascript is enabled or supported in the browser.
2. If javascript is disabled, it will show a disclaimer message on screen.

## Requirements

N/A

## Installation

Like any other drupal module install this module from the drupal administration
modules page. For further information:
https://drupal.org/documentation/install/modules-themes/modules-7
